import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { PublicacionService } from './publicaciones/publicacion.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule } from '@angular/material';
import { FormsModule } from '@angular/forms'
import { MatTableModule } from '@angular/material/table';
import { MaterialModule } from './material-module';
import { TablaPublicaciones } from './publicaciones/publicaciones.component';
import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
    MaterialModule
  ],
  providers: [PublicacionService],
  entryComponents: [TablaPublicaciones],
  declarations: [TablaPublicaciones, TimeAgoPipe],
  bootstrap: [TablaPublicaciones],
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
