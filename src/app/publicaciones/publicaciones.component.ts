import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { PublicacionService } from './publicacion.service';
/**
 * @title Table with pagination
 */
@Component({
  selector: 'tabla-publicaciones',
  styleUrls: ['publicaciones.component.css'],
  templateUrl: 'publicaciones.component.html',
})
export class TablaPublicaciones implements OnInit {
  displayedColumns: string[] = ['titulo', 'autor', 'fecha', 'id'];
  dataSource = new MatTableDataSource<Publicacion>();
  publicaciones: Publicacion[] = [];
  
  constructor(
    protected publicacionService: PublicacionService,
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  ngOnInit() {
    this.publicacionService.getPublicaciones()
    .subscribe(
      (publicaciones: Publicacion[]) => {
        this.publicaciones = publicaciones;
        this.dataSource = new MatTableDataSource(publicaciones);
        this.dataSource.paginator = this.paginator;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deleteUser(id) {
    this.publicacionService.deletePublicacion(id)
    .subscribe(data => {
      this.ngOnInit();
    });
  }
}

export interface Publicacion {
  id: string;
  titulo: string;
  autor: string;
  fecha: string;
  storyId: string;
}
