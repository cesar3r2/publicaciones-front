import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {

  constructor(protected http: HttpClient) { }

  getPublicaciones() {
    const url = "http://localhost:8080/api/v1/publicaciones/all";
    return this.http.get(url);
  }

  deletePublicacion(id) {
    const url = "http://localhost:8080/api/v1/publicaciones/delete/"+id;
    return this.http.delete(url, { responseType: 'text' });
  }
}
