# Publicaciones Hacker News
Este sitio corresponde a un Test de Frontend para Java Develover Full Stack. Se muestra un listado de publicaciones desde el sitio Hacker News.

## Lenguajes/Herramientas usadas
- Angular 7
- Angular CLI 7.3.9.
- Angular Material
- TypeScript

## Instalación y ejecución
IMPORTANTE: El serivicio de backend debe estar ejecutandose en http://localhost:8080

1. Clonar el repositorio.
git clone https://cesar3r2@bitbucket.org/cesar3r2/publicaciones-front.git

2. Una vez clonado, desde el IDE de tu preferencia o consola, cambiarte a la carpeta del proyecto ejecutar los comandos:
- npm install
- npm start

El frontend de la aplicación estará disponible en http://localhost:4200.

3. Iniciar desde el navegador la URL: http://localhost:4200
